package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.Emoji
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.UserInfoRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.toInfoSettingsState
import ru.futurobot.bot.domain.toKeyboard
import javax.inject.Inject
import javax.inject.Named

internal class SettingsCommandHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val userInfoService: UserInfoRepository,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("settings", botName) {

    override fun handleCommand(
        isGroup: Boolean, payload: String, message: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        analyticsService.track(message.chat.id, message.from.id, command, payload)
        if (!isGroup) {
            messageService?.sendMessage(
                message.chat.id,
                "Мои настройки поведения действуют только для чатов. У тебя в личке я могу творить что душе угодно " + Emoji.WINKING_FACE
            )
            return true
        }

        val admins = userInfoService.getAdmins(message.chat.id)
        if (admins.none { it.user.id == message.from.id.toInt() }) {
            messageService?.sendReplyMessage(
                message.chat.id, message.messageId,
                "Молодой человек, это не для вас сделано."
            )
            return true
        }

        // Generate settings keyboard and send it
        val settingsState = setting.toInfoSettingsState()
        val keyboard = settingsState.toKeyboard()
        messageService?.sendKeyboard(message.chat.id, keyboard)
        return true
    }
}