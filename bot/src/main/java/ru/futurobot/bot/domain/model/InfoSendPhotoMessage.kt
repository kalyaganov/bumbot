package ru.futurobot.bot.domain.model

data class InfoSendPhotoMessage(val fileId: String)