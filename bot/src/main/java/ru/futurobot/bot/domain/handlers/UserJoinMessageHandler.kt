package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.JoinPhraseRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import javax.inject.Inject

class UserJoinMessageHandler
@Inject constructor(
    private val joinPhraseRepository: JoinPhraseRepository,
    private val analyticsService: AnalyticsService
) : MessageHandler {
    override fun handle(message: Message, setting: SettingEntity, messageService: MessageService?): Boolean {
        if (message.newChatMembers.isNullOrEmpty()) return false
        if (!setting.isGreetingsEnabled)
            return true
        analyticsService.track(message.chat.id, message.from.id, "UserEntity join", "")
        val greetingPhrase = joinPhraseRepository.getPhrase() ?: return false
        messageService?.sendReplyMessage(message.chat.id, message.messageId, greetingPhrase.phrase)
        return true
    }
}