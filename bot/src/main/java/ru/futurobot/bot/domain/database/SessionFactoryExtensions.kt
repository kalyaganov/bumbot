package ru.futurobot.bot.domain.database

import org.hibernate.Session
import org.hibernate.SessionFactory

internal fun <T> SessionFactory.readTransaction(body: Session.() -> T): T {
    val target: T
    val session = openSession()
    try {
        session.transaction.begin()
        target = body.invoke(session)
        session.transaction.commit()
        return target
    } catch (ex: Exception) {
        session.transaction.rollback()
        throw ex
    } finally {
        session.close()
    }
}

internal fun SessionFactory.writeTransaction(body: Session.() -> Unit) {
    val session = openSession()
    try {
        session.transaction.begin()
        body.invoke(session)
        session.transaction.commit()
    } catch (ex: Exception) {
        session.transaction.rollback()
        throw ex
    } finally {
        session.close()
    }
}