package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.FileEntity
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.BoobsRepository
import ru.futurobot.bot.domain.repository.FileRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.DownloadingService
import ru.futurobot.bot.domain.service.MessageService
import java.util.regex.Pattern
import javax.inject.Inject

internal class BoobsMessageHandler
@Inject constructor(
    private val analyticsService: AnalyticsService,
    private val downloadingService: DownloadingService,
    private val fileRepository: FileRepository,
    private val boobsRepository: BoobsRepository
) : TextMessageHandler() {

    private val pattern = Pattern.compile(
        "((сис(ек|ьки|ечки|и|яндры|юл[еи]чки))|(ти(тьки|течки|тюли|ти|тяндры)))",
        Pattern.CASE_INSENSITIVE or Pattern.UNICODE_CASE
    )

    override fun handleMessage(
        isGroup: Boolean, text: String, messageInfo: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!pattern.matcher(text).find()) {
            return false
        }
        analyticsService.track(messageInfo.chat.id, messageInfo.from.id, "Boobs command")
        if (!setting.isBoobsEnabled) {
            return false
        }

        val boob = boobsRepository.get() ?: return false
        val imageUrl = boob.imageUrl
        val file = fileRepository.findByKey(imageUrl)
        if (file != null) {
            messageService?.sendPhoto(
                messageInfo.chat.id,
                file.id,
                if (boob.modelName.isNullOrEmpty()) "" else "Модель: " + boob.modelName
            )
        } else {
            //fetch photo and send
            val modelName = if (boob.modelName.isNullOrEmpty()) "" else "Модель: " + boob.modelName
            downloadingService.getInputStream(imageUrl).use { inputStream ->
                val infoSendPhotoMessage = messageService?.sendPhoto(
                    messageInfo.chat.id, imageUrl, inputStream, modelName
                )
                val newBoobFile = FileEntity(infoSendPhotoMessage!!.fileId, imageUrl)
                fileRepository.create(newBoobFile)
            }
        }
        return true
    }
}