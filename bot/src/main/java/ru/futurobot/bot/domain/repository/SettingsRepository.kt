package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.ChatEntity
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.database.SettingsDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingsRepository
@Inject constructor(
    private val settingsDataSource: SettingsDataSource
) {
    fun get(chat: ChatEntity): SettingEntity = settingsDataSource.readByChatId(chat.id) ?: SettingEntity(chat = chat)

    fun update(setting: SettingEntity) = settingsDataSource.createOrUpdate(setting)
}