package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.GreetingsPhraseEntity
import ru.futurobot.bot.domain.database.GreetingsPhraseDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class JoinPhraseRepository
@Inject constructor(
    private val phraseDataSource: GreetingsPhraseDataSource
) {
    fun getPhrase(chatId: Long = 0L): GreetingsPhraseEntity? = phraseDataSource.readRandomPhraseByPower(chatId)
}

