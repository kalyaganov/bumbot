package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.PhraseType
import ru.futurobot.bot.data.entity.VotePhraseEntity
import ru.futurobot.bot.domain.database.VotePhraseDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VotePhraseRepository
@Inject constructor(
    private val votePhraseDataSource: VotePhraseDataSource
) {
    fun getPhrase(type: PhraseType): VotePhraseEntity? = votePhraseDataSource.readRandomPhrase(type)
}