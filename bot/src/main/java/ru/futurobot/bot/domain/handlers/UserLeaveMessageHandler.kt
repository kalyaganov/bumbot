package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.LeavePhraseRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import javax.inject.Inject

class UserLeaveMessageHandler
@Inject constructor(
    private val leavePhraseRepository: LeavePhraseRepository,
    private val analyticsService: AnalyticsService
) : MessageHandler {
    override fun handle(message: Message, setting: SettingEntity, messageService: MessageService?): Boolean {
        if (message.leftChatMember == null) return false
        if (!setting.isByeEnabled) return true
        analyticsService.track(message.chat.id, message.from.id, "UserEntity leave", "")
        val leavePhrase = leavePhraseRepository.getPhrase() ?: return false
        messageService?.sendReplyMessage(message.chat.id, message.messageId, leavePhrase.phrase)
        return true
    }
}