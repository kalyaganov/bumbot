package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.MessageService


/**
 * Created by Alexey on 03.04.2018. Futurobot
 */
abstract class CommandMessageHandler(
    protected val command: String,
    protected val botUsername: String
) : TextMessageHandler() {

    private val fullCommand = "/$command@$botUsername"
    private val compactCommand = "/$command"

    protected abstract fun handleCommand(
        isGroup: Boolean,
        payload: String,
        message: Message,
        setting: SettingEntity,
        messageService: MessageService?
    ): Boolean

    /***
     * command message must be start with /command@bot_name, or /command if it is not in the group
     */
    override fun handleMessage(
        isGroup: Boolean,
        text: String,
        messageInfo: Message,
        setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (text.startsWith(this.fullCommand, true)) {
            return handleCommand(isGroup, getPayload(fullCommand, text), messageInfo, setting, messageService)
        } else if (!isGroup && text.startsWith(compactCommand, true)) {
            return handleCommand(isGroup, getPayload(compactCommand, text), messageInfo, setting, messageService)
        }
        return false
    }


    private fun getPayload(commandPattern: String, text: String): String {
        if (text.length == commandPattern.length) {
            return ""
        }
        return text.substring(commandPattern.length).trim()
    }
}
