package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.UserEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) : DbDataSource<UserEntity>(sessionFactory, logger) {

    fun readById(userId: Long): UserEntity? = sessionFactory.readTransaction {
        session.createQuery("FROM UserEntity U WHERE U.id = :userId")
            .setParameter("userId", userId)
            .setMaxResults(1)
            .list().firstOrNull() as? UserEntity
    }
}