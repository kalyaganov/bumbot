package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.LeavePhraseEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LeavePhraseDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) :
    DbDataSource<LeavePhraseEntity>(sessionFactory, logger) {

    fun readByChatId(chatId: Long = 0L): LeavePhraseEntity? = sessionFactory.readTransaction {
        val query = if (chatId == 0L)
            session.createQuery("FROM LeavePhraseEntity LP ORDER BY RAND()")
        else
            session.createQuery("FROM LeavePhraseEntity LP WHERE LP.chat.id = :chatId ORDER BY RAND()")
                .setParameter("chatId", chatId)
        query.setMaxResults(1).list().firstOrNull() as? LeavePhraseEntity
    }
}