package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.ChatEntity
import ru.futurobot.bot.domain.database.ChatDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatRepository
@Inject constructor(
    private val chatDataSource: ChatDataSource
) {
    fun get(id: Long): ChatEntity? = chatDataSource.readById(id)

    fun addOrUpdate(chat: ChatEntity) = chatDataSource.createOrUpdate(chat)
}

