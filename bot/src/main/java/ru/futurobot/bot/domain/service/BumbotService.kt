package ru.futurobot.bot.domain.service

import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.ChatEntity
import ru.futurobot.bot.domain.handlers.CallbackQueryMessageHandlers
import ru.futurobot.bot.domain.handlers.TextMessageHandlers
import ru.futurobot.bot.domain.repository.ChatRepository
import ru.futurobot.bot.domain.repository.SettingsRepository
import ru.futurobot.bot.domain.repository.StatisticRepository
import ru.futurobot.bot.domain.repository.UserRepository
import ru.futurobot.bot.domain.toEntity
import ru.futurobot.bot.domain.update
import java.util.logging.Level
import java.util.logging.Logger
import javax.inject.Inject

/**
 * Bumbot service
 */
class BumbotService
@Inject constructor(
    private val messageHandlers: TextMessageHandlers,
    private val callbackQueryHandlers: CallbackQueryMessageHandlers,
    private val settingsRepository: SettingsRepository,
    private val chatRepository: ChatRepository,
    private val userRepository: UserRepository,
    private val statisticRepository: StatisticRepository,
    private val logger: Logger
) {

    fun handleMessage(message: Message, messageService: MessageService) {
        try {
            chatRepository.addOrUpdate(message.chat.toEntity())
            userRepository.addOrUpdate(message.from.toEntity())
            message.replyToMessage?.from?.let { userRepository.addOrUpdate(it.toEntity()) }

            var statistics = statisticRepository.getStatistic(message.chat.toEntity(), message.from.toEntity())
            statistics = statistics.update(message)
            statisticRepository.addOrUpdate(statistics)

            val setting = settingsRepository.get(message.chat.toEntity())

            for (handler in messageHandlers) {
                if (handler.handle(message, setting, messageService)) {
                    if (!handler.isOtherHandlersAllowed) {
                        break
                    }
                }
            }
        } catch (e: Exception) {
            logger.log(
                Level.WARNING,
                "Error while handling text message $message \n${e.message}\n${e.printStackTrace()}"
            )
        }
    }

    fun handleQueryMessage(message: CallbackQuery, messageService: MessageService) {
        try {
            val setting = settingsRepository.get(ChatEntity(id = message.message.chatId.toLong()))
            for (handler in callbackQueryHandlers) {
                if (handler.handle(message, setting, messageService)) {
                    if (!handler.isOtherHandlersAllowed) {
                        break
                    }
                }
            }
        } catch (e: Exception) {
            logger.log(
                Level.WARNING, "Error while handling query message $message \n${e.message}\n" +
                        "\${e.printStackTrace()"
            )
        }
    }
}