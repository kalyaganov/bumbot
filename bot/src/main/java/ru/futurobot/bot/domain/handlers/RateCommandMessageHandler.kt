package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.Emoji
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import javax.inject.Inject
import javax.inject.Named

internal class RateCommandMessageHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("rate", botName) {

    private val rateMessages = arrayOf(
        "0/10 Полный отстой!",
        "1/10 Дрисня " + Emoji.POOP,
        "2/10 Блевотина",
        "3/10 Хреново",
        "4/10 Нууу, такое",
        "5/10 Средненько",
        "6/10 На любителя, но мне нравится",
        "7/10 Очень даже не плохо",
        "8/10 Ах, хорошо! " + Emoji.THUMBS_UP_SIGN,
        "9/10 Здорово " + Emoji.SMILING_FACE_WITH_OPEN_MOUTH,
        "О, БОЖЕ! 10/10 " + Emoji.HANDS_CLAPPING
    )

    override fun handleCommand(
        isGroup: Boolean,
        payload: String,
        message: Message,
        setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        analyticsService.track(message.chat.id, message.from.id, command, "")
        when {
            message.replyToMessage?.from != null ->
                messageService?.sendReplyMessage(
                    message.chat.id,
                    message.replyToMessage.messageId,
                    rateMessages.random()
                )
            payload.isNotBlank() ->
                messageService?.sendReplyMessage(
                    message.chat.id,
                    message.messageId,
                    "$payload - ${rateMessages.random()}"
                )
            else -> messageService?.sendReplyMessage(message.chat.id, message.messageId, rateMessages.random())
        }
        return true
    }
}