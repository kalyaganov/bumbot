package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.FileEntity
import ru.futurobot.bot.domain.database.FileDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class FileRepository
@Inject constructor(
    private val fileDataSource: FileDataSource
) {
    fun findByKey(key: String): FileEntity? = fileDataSource.readByKey(key)
    fun create(file: FileEntity) = fileDataSource.createOrUpdate(file)
}