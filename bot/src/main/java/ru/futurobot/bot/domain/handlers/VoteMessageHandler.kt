package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.PhraseType
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.RatingRepository
import ru.futurobot.bot.domain.repository.VotePhraseRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.toEntity
import java.time.Instant
import java.util.*
import java.util.logging.Logger
import kotlin.math.abs
import kotlin.math.log10
import kotlin.math.max

/**
 * Created by Alexey on 03.04.2018. Futurobot
 */
abstract class VoteMessageHandler constructor(
    private val analyticsService: AnalyticsService,
    private val ratingRepository: RatingRepository,
    private val votePhraseRepository: VotePhraseRepository,
    private val logger: Logger
) : TextMessageHandler() {

    abstract fun detectPattern(text: String): Boolean

    abstract fun parseVotePower(text: String): Int

    override fun handleMessage(
        isGroup: Boolean, text: String, messageInfo: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!(messageInfo.isReply && messageInfo.replyToMessage.chat.id == messageInfo.chat.id && messageInfo.replyToMessage.from.id != messageInfo.from.id)) {
            return false
        }
        if (!detectPattern(text)) {
            return false
        }
        analyticsService.track(messageInfo.chat.id, messageInfo.from.id, "Vote", "")

        val votePower = parseVotePower(text)
        val senderRating = ratingRepository.getRating(messageInfo.chat.toEntity(), messageInfo.from.toEntity())
        val receiverRating =
            ratingRepository.getRating(messageInfo.chat.toEntity(), messageInfo.replyToMessage.from.toEntity())

        when {
            senderRating.rating < 0 -> {
                val message = String.format(
                    Locale.ROOT, "Кто дал право <b>%s(%.2f)</b> голосовать?",
                    messageInfo.from.getUsernameOrFullName(), senderRating.rating
                )
                messageService?.sendMessage(messageInfo.chat.id, message, MessageService.ParseMode.Html)
            }
            senderRating.nextVoteDate.isAfter(Instant.now()) -> { /*SPAM*/
            }
            else -> {
                val extraRating = if (senderRating.rating <= 0.0f)
                    1f
                else
                    votePower * max(1f, 2f * log10(senderRating.rating.toDouble()).toFloat())
                //Update sender vote time to current
                val updatedSenderRating = senderRating.copy(
                    voteDate = Instant.now(),
                    nextVoteDate = Instant.now().plusSeconds(abs(votePower * 10).toLong())
                )
                val updatedReceiverRating = receiverRating.copy(rating = receiverRating.rating.plus(extraRating))

                ratingRepository.update(updatedSenderRating, updatedReceiverRating)

                val phraseType = if (votePower >= 0) PhraseType.Positive else PhraseType.Negative
                val phrase = votePhraseRepository.getPhrase(phraseType) ?: return false

                val userFrom = String.format(
                    Locale.ROOT, "<b>%s(%.2f)</b>",
                    messageInfo.from.getUsernameOrFullName(), updatedSenderRating.rating
                )
                val userTo = String.format(
                    Locale.ROOT, "<b>%s(%.2f)</b>",
                    messageInfo.replyToMessage.from.getUsernameOrFullName(),
                    updatedReceiverRating.rating
                )
                val voteMessage = phrase.phrase.replace("{0}", userFrom).replace("{1}", userTo)
                //Send response
                messageService?.sendMessage(messageInfo.chat.id, voteMessage, MessageService.ParseMode.Html)
            }
        }
        return true
    }
}

