package ru.futurobot.bot.domain

import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.User
import ru.futurobot.bot.data.entity.ChatEntity
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.data.entity.UserEntity
import ru.futurobot.bot.domain.model.*
import java.util.*
import kotlin.collections.ArrayList

fun Chat.toEntity(): ChatEntity = ChatEntity(
    id = this.id,
    title = this.title ?: "",
    username = this.userName ?: "",
    firstName = this.firstName ?: "",
    lastName = this.lastName ?: "",
    type = this.type(),
    leavePhraseEntitySet = emptySet(),
    votePhraseEntitySet = emptySet(),
    ratingEntitySet = emptySet(),
    greetingsPhraseEntitySet = emptySet(),
    statisticEntitySet = emptySet()
)

fun User.toEntity(): UserEntity = UserEntity(
    this.id.toLong(),
    this.userName ?: "",
    this.firstName ?: "",
    this.lastName ?: "",
    emptySet(),
    emptySet()
)

fun Message.toInfoSendPhotoMessage(): InfoSendPhotoMessage =
    InfoSendPhotoMessage(fileId = photo.first().fileId)

fun Message.toSendPhotoMessage(): InfoSendPhotoMessage? =
    if (photo.isNullOrEmpty()) null else InfoSendPhotoMessage(photo[0].fileId)

fun SettingEntity.toInfoSettingsState(): InfoSettingsState {
    val text = String.format(
        Locale.ROOT, "*Настройки настроечки*\n" +
                "Показывать сиськи: *%s*\n" +
                "Показывать попки: *%s*\n" +
                "Приветствие: *%s*\n" +
                "Прощание: *%s*\n" +
                "Ответы на вопросы y/n: *%s*\n",
        if (isBoobsEnabled) "Вкл" else "Выкл",
        if (isButtsEnabled) "Вкл" else "Выкл",
        if (isGreetingsEnabled) "Вкл" else "Выкл",
        if (isByeEnabled) "Вкл" else "Выкл",
        if (is8BallEnabled) "Вкл" else "Выкл"
    )

    val buttonList = ArrayList<InfoButton>()
    buttonList.add(
        InfoButton(
            if (!isBoobsEnabled) "Включить сиськи" else "Отключить сиськи",
            InfoSettingsState.CALLBACK_QUERY + InfoSettingsState.BOOBS_SETTINGS
        )
    )
    buttonList.add(
        InfoButton(
            if (!isButtsEnabled) "Включить попки" else "Отключить попки",
            InfoSettingsState.CALLBACK_QUERY + InfoSettingsState.BUTTS_SETTINGS
        )
    )
    buttonList.add(
        InfoButton(
            if (!isGreetingsEnabled) "Включить приветствие" else "Отключить приветствие",
            InfoSettingsState.CALLBACK_QUERY + InfoSettingsState.GREETINGS_SETTINGS
        )
    )
    buttonList.add(
        InfoButton(
            if (!isByeEnabled) "Включить прощание" else "Отключить прощание",
            InfoSettingsState.CALLBACK_QUERY + InfoSettingsState.BYE_SETTINGS
        )
    )
    buttonList.add(
        InfoButton(
            if (!is8BallEnabled) "Включить ответы" else "Отключить ответы",
            InfoSettingsState.CALLBACK_QUERY + InfoSettingsState.YES_NO_SETTINGS
        )
    )

    return InfoSettingsState(text, buttonList)
}

fun InfoSettingsState.toKeyboard(): Keyboard {
    val rowInfoList = buttons.map { InfoButtonRow(listOf(it)) }
    return Keyboard(text, rowInfoList)
}