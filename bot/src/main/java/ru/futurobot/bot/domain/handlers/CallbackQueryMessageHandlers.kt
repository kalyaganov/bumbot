package ru.futurobot.bot.domain.handlers

import javax.inject.Inject

class CallbackQueryMessageHandlers
@Inject internal constructor(
    settingsCallbackQueryHandler: SettingsCallbackQueryHandler
) : ArrayList<CallbackQueryHandler>() {
    init {
        add(settingsCallbackQueryHandler)
    }
}