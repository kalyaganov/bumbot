package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.StatisticRepository
import ru.futurobot.bot.domain.repository.UserRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.toEntity
import javax.inject.Inject
import javax.inject.Named
import kotlin.math.max
import kotlin.math.min

internal class StatCommandHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val statisticRepository: StatisticRepository,
    private val userRepository: UserRepository,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("stat", botName) {

    override fun handleCommand(
        isGroup: Boolean, payload: String, message: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!isGroup) {
            return false
        }
        analyticsService.track(message.chat.id, message.from.id, command, payload)
        val topCount = min(50, max(payload.toIntOrNull() ?: 5, 0))

        val statistics = statisticRepository.getByTopMessages(message.chat.toEntity(), topCount)
        val stringBuilder = StringBuilder("<b>Статистика - топ " + statistics.size + " чата:</b>")
        statistics.forEachIndexed { index, statistic ->
            stringBuilder
                .append("\n<b>")
                .append(index + 1)
                .append(".</b> ")
                .append(statistic.user.getAkaUserName())
                .append(" <b>${statistic.messagesCount}</b> сообщений")
        }

        messageService?.sendMessage(message.chat.id, stringBuilder.toString(), MessageService.ParseMode.Html)
        return true
    }
}

