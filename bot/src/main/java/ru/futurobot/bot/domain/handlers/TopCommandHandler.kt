package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.RatingRepository
import ru.futurobot.bot.domain.repository.UserRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.toEntity
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import kotlin.math.max
import kotlin.math.min

internal class TopCommandHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val ratingRepository: RatingRepository,
    private val userRepository: UserRepository,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("top", botName) {

    override fun handleCommand(
        isGroup: Boolean, payload: String, message: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!isGroup) {
            return false
        }
        analyticsService.track(message.chat.id, message.from.id, command, payload)
        val topCount = min(50, max(payload.toIntOrNull() ?: 5, 0))

        val ratings = ratingRepository.getTop(message.chat.toEntity(), topCount)
        if (ratings.isEmpty()) {
            messageService?.sendMessage(message.chat.id, "У вас тут никто ещё не голосовал")
        } else {
            val stringBuilder = StringBuilder("<b>Топ " + ratings.size + " рейтинговых ребят:</b>")
            ratings.forEachIndexed { index, rating ->
                stringBuilder
                    .append("\n<b>")
                    .append(index + 1)
                    .append(".</b> ")
                    .append(rating.user.getAkaUserName())
                    .append(String.format(Locale.ROOT, " <b>(%.2f)</b>", rating.rating))
            }
            messageService?.sendMessage(message.chat.id, stringBuilder.toString(), MessageService.ParseMode.Html)
        }
        return true
    }
}