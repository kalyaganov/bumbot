package ru.futurobot.bot.domain

import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.StatisticEntity

fun Chat.type(): Int = when {
    this.isUserChat -> 0
    this.isGroupChat -> 1
    this.isSuperGroupChat -> 2
    this.isChannelChat -> 3
    else -> -1
}

fun StatisticEntity.update(infoMessage: Message): StatisticEntity =
    this.copy(
        messagesCount = messagesCount + 1,
        stickersCount = if (infoMessage.hasSticker()) stickersCount + 1 else stickersCount,
        textsCount = if (infoMessage.hasText()) textsCount + 1 else textsCount,
        imagesCount = if (infoMessage.hasPhoto()) imagesCount + 1 else imagesCount,
        documentsCount = if (infoMessage.hasDocument()) documentsCount + 1 else documentsCount,
        audiosCount = if (infoMessage.audio != null) audiosCount + 1 else audiosCount,
        videosCount = if (infoMessage.hasVideo()) videosCount + 1 else videosCount,
        voicesCount = if (infoMessage.voice != null) voicesCount + 1 else voicesCount
    )