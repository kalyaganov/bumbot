package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import java.util.logging.Logger

abstract class DbDataSource<T>(
    protected val sessionFactory: SessionFactory,
    private val logger: Logger
) {

    fun createOrUpdate(vararg entities: T) {
        sessionFactory.writeTransaction { entities.forEach { saveOrUpdate(it) } }
    }
}