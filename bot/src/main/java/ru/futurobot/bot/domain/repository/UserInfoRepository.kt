package ru.futurobot.bot.domain.repository

import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators
import org.telegram.telegrambots.meta.api.objects.ChatMember
import org.telegram.telegrambots.meta.bots.AbsSender
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import java.util.logging.Logger
import javax.inject.Inject

class UserInfoRepository
@Inject constructor(
    private val absSender: AbsSender,
    private val logger: Logger
) {
    fun getAdmins(chatId: Long): List<ChatMember> {
        try {
            return absSender.execute(GetChatAdministrators().setChatId(chatId))
        } catch (e: TelegramApiException) {
            logger.warning("Exception while getting admins: " + e.message)
        }
        return emptyList()
    }
}