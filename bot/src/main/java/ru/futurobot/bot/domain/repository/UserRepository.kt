package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.UserEntity
import ru.futurobot.bot.domain.database.UserDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository
@Inject constructor(
    private val userDataSource: UserDataSource
) {
    fun get(id: Long): UserEntity? = userDataSource.readById(id)

    fun addOrUpdate(user: UserEntity) = userDataSource.createOrUpdate(user)
}