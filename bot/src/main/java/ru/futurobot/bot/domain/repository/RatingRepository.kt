package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.ChatEntity
import ru.futurobot.bot.data.entity.RatingEntity
import ru.futurobot.bot.data.entity.UserEntity
import ru.futurobot.bot.domain.database.RatingDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RatingRepository
@Inject constructor(
    private val ratingDataSource: RatingDataSource
) {
    fun getRating(chat: ChatEntity, user: UserEntity): RatingEntity =
        ratingDataSource.readUserRating(chat.id, user.id) ?: RatingEntity(chat = chat, user = user)

    fun getTop(chat: ChatEntity, topCount: Int): Collection<RatingEntity> =
        ratingDataSource.readTopRatings(chat.id, topCount)

    fun update(vararg ratings: RatingEntity) = ratingDataSource.createOrUpdate(*ratings)
}