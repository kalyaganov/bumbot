package ru.futurobot.bot.domain.model

data class InfoButton(val text: String, val callbackData: String)