package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.Boob
import ru.futurobot.bot.domain.model.data.OboobsApi
import javax.inject.Inject

class BoobsRepository @Inject constructor(private val oboobsApi: OboobsApi) {
    fun get(): Boob? {
        val boobs = oboobsApi.boob().execute()
        if (boobs.isSuccessful) {
            return boobs.body()!![0]
        }
        throw IllegalStateException("Retrofit oboobs service returned nothing ${boobs.message()}")
    }
}