package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.MessageService

abstract class AvatarChangedMessageHandler : MessageHandler {
    override fun handle(message: Message, setting: SettingEntity, messageService: MessageService?): Boolean {
        if (message.newChatPhoto.isNullOrEmpty()) {
            return false
        }
        val isGroup = message.isGroupMessage || message.isSuperGroupMessage
        return handleMessage(isGroup, message, messageService)
    }

    protected abstract fun handleMessage(
        isGroup: Boolean,
        messageInfo: Message,
        messageService: MessageService?
    ): Boolean
}