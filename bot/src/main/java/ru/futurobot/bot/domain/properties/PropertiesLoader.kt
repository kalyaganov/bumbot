package ru.futurobot.bot.domain.properties

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.*

/**
 * Created by kenny on 18.06.2018. Futurobot
 * Loads properties in the following order:
 * 1. Property file specified in arguments when running (--properties=prop.file)
 * 2. Local file with properties. Must be named as **bumbot.properties**
 * 3. Properties from resources (resources/application.properties)
 */
class PropertiesLoader(private val arguments: Array<String>) {

    @Throws(IOException::class)
    fun load(): Properties = when {
        hasPropertiesInArgs() -> loadPropertiesFromArgsFile()
        hasPropertiesInLocalFile() -> loadPropertiesFromLocalFile()
        hasPropertiesInResource() -> loadPropertiesFromResources()
        else -> throw IllegalStateException("Properties file not found.")
    }

    /**
     * Load properties from resources
     */
    @Throws(IOException::class)
    private fun loadPropertiesFromResources(): Properties {
        try {
            ClassLoader.getSystemResourceAsStream(PropertiesFileResources).use { inputStream ->
                val properties = Properties()
                properties.load(inputStream)
                return properties
            }
        } catch (e: IOException) {
            throw IOException(
                "Cannot load properties resource from $PropertiesFileResources: ${e.message}"
            )
        }

    }

    @Throws(IOException::class)
    private fun loadPropertiesFromLocalFile(): Properties = loadPropertiesFromFile(PropertiesFileLocal)

    @Throws(IOException::class)
    private fun loadPropertiesFromArgsFile(): Properties = loadPropertiesFromFile(readArgument(ArgumentPropertiesFile))

    /**
     * Load properties from file
     */
    @Throws(IOException::class)
    private fun loadPropertiesFromFile(fileName: String): Properties {
        try {
            FileInputStream(fileName).use { inputStream ->
                val properties = Properties()
                properties.load(inputStream)
                return properties
            }
        } catch (e: IOException) {
            throw IOException(
                "Cannot load properties from file " + fileName + ": " + e.message
            )
        }
    }

    private fun hasPropertiesInArgs(): Boolean = readArgument(ArgumentPropertiesFile) != ""

    private fun hasPropertiesInLocalFile(): Boolean {
        val file = File(PropertiesFileLocal)
        return file.exists() && file.canRead()
    }

    private fun hasPropertiesInResource(): Boolean {
        try {
            return ClassLoader.getSystemResource(PropertiesFileResources) != null
        } catch (e: IOException) {
        }
        return false
    }

    private fun readArgument(argumentName: String): String {
        val argsIterator = listOf(*arguments).iterator()
        while (argsIterator.hasNext()) {
            val arg = argsIterator.next()
            if (arg.startsWith("--$argumentName=")) {
                return arg.substring("--$argumentName=".length)
            } else if (arg == "-$argumentName") {
                if (argsIterator.hasNext()) {
                    return argsIterator.next()
                }
            }
        }
        return ""
    }

    companion object {
        private const val ArgumentPropertiesFile = "properties"
        private const val PropertiesFileLocal = "bumbot.properties"
        private const val PropertiesFileResources = "application.properties"
    }
}
