package ru.futurobot.bot.domain.service

import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.meta.bots.AbsSender
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import ru.futurobot.bot.domain.model.InfoSendPhotoMessage
import ru.futurobot.bot.domain.model.Keyboard
import ru.futurobot.bot.domain.service.exception.SendPhotoException
import ru.futurobot.bot.domain.toInfoSendPhotoMessage
import java.io.InputStream
import java.util.*
import java.util.logging.Logger
import javax.inject.Inject

class TelegramMessageService
@Inject constructor(
    private val absSender: AbsSender,
    private val logger: Logger
) : MessageService {
    override fun sendMessage(chatId: Long, message: String, parseMode: MessageService.ParseMode) {
        try {
            val sendMessage = SendMessage()
                .setChatId(chatId)
                .setText(message)
                .setParseMode(parseMode.mode)
            absSender.execute(sendMessage)
        } catch (e: TelegramApiException) {
            logger.throwing(this.javaClass.name, "sendMessage", e)
        }
    }

    override fun sendReplyMessage(
        chatId: Long, replyMessageId: Int, message: String,
        parseMode: MessageService.ParseMode
    ) {
        try {
            absSender.execute(
                SendMessage().setChatId(chatId)
                    .setReplyToMessageId(replyMessageId)
                    .setParseMode(parseMode.mode)
                    .setText(message)
            )
        } catch (e: TelegramApiException) {
            logger.throwing(this.javaClass.name, "sendMessage", e)
        }
    }

    @Throws(SendPhotoException::class)
    override fun sendPhoto(
        chatId: Long, url: String, inputStream: InputStream,
        caption: String
    ): InfoSendPhotoMessage {
        try {
            val message = absSender.execute(
                SendPhoto()
                    .setChatId(chatId)
                    .setPhoto(url, inputStream)
                    .setCaption(caption)
            )
            return message.toInfoSendPhotoMessage()
        } catch (e: TelegramApiException) {
            throw SendPhotoException("Send new photo error: " + e.message)
        }

    }

    @Throws(SendPhotoException::class)
    override fun sendPhoto(chatId: Long, fileId: String, caption: String): InfoSendPhotoMessage {
        try {
            val sendPhoto = SendPhoto()
                .setChatId(chatId)
                .setPhoto(fileId)
            if (caption != "") {
                sendPhoto.caption = caption
            }
            val message = absSender.execute(sendPhoto)
            return message.toInfoSendPhotoMessage()
        } catch (e: TelegramApiException) {
            throw SendPhotoException("Send new photo error: ${e.message}\n${e.printStackTrace()}")
        }

    }

    override fun sendKeyboard(chatId: Long, keyboard: Keyboard) {
        val markup = createKeyboardMarkup(keyboard)
        try {
            absSender.execute(
                SendMessage()
                    .setChatId(chatId)
                    .setText(keyboard.title)
                    .setReplyMarkup(markup)
                    .setParseMode(MessageService.ParseMode.Markdown.mode)
            )
        } catch (e: TelegramApiException) {
            logger.throwing(this.javaClass.name, "sendKeyboard", e)
        }

    }

    override fun editKeyboard(
        chatId: Long,
        messageId: Int,
        keyboard: Keyboard
    ) {
        val markup = createKeyboardMarkup(keyboard)

        try {
            absSender.execute(
                EditMessageText()
                    .setMessageId(messageId)
                    .setChatId(chatId)
                    .setText(keyboard.title)
                    .setReplyMarkup(markup)
                    .setParseMode(MessageService.ParseMode.Markdown.mode)
            )
        } catch (e: TelegramApiException) {
            logger.throwing(this.javaClass.name, "sendKeyboard", e)
        }

    }

    private fun createKeyboardMarkup(keyboard: Keyboard): InlineKeyboardMarkup {
        val tgKeyboard =
            ArrayList<List<InlineKeyboardButton>>()
        for ((buttonInfoList) in keyboard.infoButtonRowList) {
            val row =
                ArrayList<InlineKeyboardButton>()
            for ((text, callbackData) in buttonInfoList) {
                val button = InlineKeyboardButton()
                button.text = text
                button.callbackData = callbackData
                row.add(button)
            }
            tgKeyboard.add(row)
        }

        val markup = InlineKeyboardMarkup()
        markup.keyboard = tgKeyboard
        return markup
    }
}