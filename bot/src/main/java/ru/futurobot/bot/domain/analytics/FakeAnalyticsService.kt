package ru.futurobot.bot.domain.analytics

import ru.futurobot.bot.domain.service.AnalyticsService
import javax.inject.Inject

class FakeAnalyticsService @Inject constructor() : AnalyticsService {
    override fun track(chatId: Long, userId: Int, group: String, message: String) {

    }
}