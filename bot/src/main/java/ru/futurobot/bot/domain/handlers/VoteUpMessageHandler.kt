package ru.futurobot.bot.domain.handlers

import ru.futurobot.bot.domain.repository.RatingRepository
import ru.futurobot.bot.domain.repository.VotePhraseRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import java.util.logging.Logger
import java.util.regex.Pattern
import javax.inject.Inject
import kotlin.math.min

internal class VoteUpMessageHandler
@Inject constructor(
    analyticsService: AnalyticsService,
    ratingRepository: RatingRepository,
    votePhraseRepository: VotePhraseRepository,
    logger: Logger
) : VoteMessageHandler(analyticsService, ratingRepository, votePhraseRepository, logger) {
    private val votePattern = Pattern.compile("^[+]*", Pattern.CASE_INSENSITIVE or Pattern.UNICODE_CASE)

    override fun parseVotePower(text: String): Int {
        val matcher = votePattern.matcher(text)
        if (matcher.find()) return min(6, matcher.group(0).length)
        return 0
    }

    override fun detectPattern(text: String): Boolean = text.startsWith("+")
}