package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import javax.inject.Inject
import javax.inject.Named

internal class StartCommandHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("start", botName) {

    private val groupMessage = "Привет, дружок! Меня зовут Бороздобородый, я буду тебя развлекать.\n" +
            "Надеюсь тебе есть 18? Иначе лучше нам не общаться, а то мамка заругает.\n" +
            "Чтобы узнать что я умею отправь /help"

    private val privateMessage = (
            "Здарова, бандиты! Меня зовут Бороздобородый, я буду вас развлекать.\n"
                    + "Надеюсь вам тут всем по 18 исполнилось? Если нет, то попрошу лишних удалиться.\n"
                    + "Чтобы узнать что я умею отправьте /help")

    override fun handleCommand(
        isGroup: Boolean,
        payload: String,
        message: Message,
        setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        analyticsService.track(message.chat.id, message.from.id, command, payload)
        if (payload.isNotBlank()) {
            // authorization?
        } else {
            val msg = if (isGroup) groupMessage else privateMessage
            messageService?.sendMessage(message.chatId, msg, MessageService.ParseMode.Html)
        }
        return true
    }
}

