package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.Emoji
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.MessageService
import java.util.*
import java.util.regex.Pattern
import javax.inject.Inject

internal class EightBallMessageHandler @Inject constructor() : TextMessageHandler() {
    private val yesAnswers = arrayOf(
        "Да",
        "Абсолютно!",
        "Конечно " + Emoji.THUMBS_UP_SIGN,
        "Без сомнений",
        "Ещё бы " + Emoji.WINKING_FACE,
        "Угу",
        "Absolutely.",
        "Базаришь!",
        "Да, мой господин."
    )
    private val noAnswers = arrayOf(
        "Нет",
        "Ни в коем случае!",
        "Абсолютно исключено " + Emoji.FACE_WITH_NO_GOOD_GESTURE,
        "Даже не думай!",
        "Неа",
        "In your dreams.",
        "В твоих влажных мечтах.",
        "Нет, мой господин."
    )
    private val random: Random = Random(System.currentTimeMillis())

    private val pattern = Pattern.compile(
        "(^|([ ]*))(y|yes|д|да)/(n|no|нет|н)($|([ ]*))",
        Pattern.CASE_INSENSITIVE or Pattern.UNICODE_CASE
    )

    override fun handleMessage(
        isGroup: Boolean, text: String, messageInfo: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!setting.is8BallEnabled)
            return false

        val triggered = pattern.matcher(text).find() || (messageInfo.isReply && text == "?")
        if (!triggered) {
            return false
        }
        val answer = if (random.nextBoolean()) yesAnswers.random() else noAnswers.random()
        messageService?.sendReplyMessage(messageInfo.chat.id, messageInfo.messageId, answer)
        return true
    }
}

