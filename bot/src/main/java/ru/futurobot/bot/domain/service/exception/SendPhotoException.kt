package ru.futurobot.bot.domain.service.exception

/**
 * Created by Alexey on 06.04.2018. Futurobot
 */
class SendPhotoException(message: String) : Exception(message)
