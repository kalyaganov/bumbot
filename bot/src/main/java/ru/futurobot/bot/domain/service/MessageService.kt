package ru.futurobot.bot.domain.service

import ru.futurobot.bot.domain.model.InfoSendPhotoMessage
import ru.futurobot.bot.domain.model.Keyboard
import ru.futurobot.bot.domain.service.exception.SendPhotoException
import java.io.InputStream

/**
 * Created by Alexey on 03.04.2018. Futurobot
 */
interface MessageService {
    fun sendMessage(chatId: Long, message: String, parseMode: ParseMode = ParseMode.Text)

    fun sendReplyMessage(chatId: Long, replyMessageId: Int, message: String, parseMode: ParseMode = ParseMode.Text)

    fun sendKeyboard(chatId: Long, keyboard: Keyboard)

    fun editKeyboard(chatId: Long, messageId: Int, keyboard: Keyboard)

    @Throws(SendPhotoException::class)
    fun sendPhoto(chatId: Long, url: String, inputStream: InputStream, caption: String): InfoSendPhotoMessage

    @Throws(SendPhotoException::class)
    fun sendPhoto(chatId: Long, fileId: String, caption: String): InfoSendPhotoMessage


    enum class ParseMode(val mode: String) {
        Html("html"),
        Text(""),
        Markdown("markdown")
    }
}

