package ru.futurobot.bot.domain.model

data class InfoButtonRow(var infoButtonList: List<InfoButton>)