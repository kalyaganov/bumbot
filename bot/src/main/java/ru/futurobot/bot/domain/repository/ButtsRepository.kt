package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.Butt
import ru.futurobot.bot.domain.model.data.ObuttsApi
import javax.inject.Inject

class ButtsRepository @Inject constructor(private val obuttsApi: ObuttsApi) {

    fun get(): Butt? {
        val butts = obuttsApi.butt().execute()
        if (butts.isSuccessful) {
            return butts.body()!![0]
        }
        throw IllegalStateException("Retrofit obutts service returned nothing ${butts.message()}")
    }
}