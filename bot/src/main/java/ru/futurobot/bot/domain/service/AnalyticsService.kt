package ru.futurobot.bot.domain.service

/**
 * Created by Alexey on 03.04.2018. Futurobot
 */
interface AnalyticsService {
    fun track(chatId: Long, userId: Int, group: String, message: String = "")
}