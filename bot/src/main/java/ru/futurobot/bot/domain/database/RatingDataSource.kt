package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.RatingEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RatingDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) :
    DbDataSource<RatingEntity>(sessionFactory, logger) {

    fun readTopRatings(chatId: Long, count: Int): Collection<RatingEntity> = sessionFactory.readTransaction {
        session.createQuery("FROM RatingEntity R WHERE R.chat.id = :chatId ORDER BY R.rating DESC ")
            .setParameter("chatId", chatId)
            .setMaxResults(count)
            .list()
            .map { it as RatingEntity }
    }

    fun readUserRating(chatId: Long, userId: Long): RatingEntity? = sessionFactory.readTransaction {
        session.createQuery("FROM RatingEntity R WHERE R.chat.id = :chatId AND R.user.id = :userId")
            .setParameter("chatId", chatId)
            .setParameter("userId", userId)
            .setMaxResults(1)
            .list().firstOrNull() as? RatingEntity
    }
}