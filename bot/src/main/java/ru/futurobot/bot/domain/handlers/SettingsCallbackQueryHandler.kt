package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.model.InfoSettingsState.Companion.BOOBS_SETTINGS
import ru.futurobot.bot.domain.model.InfoSettingsState.Companion.BUTTS_SETTINGS
import ru.futurobot.bot.domain.model.InfoSettingsState.Companion.BYE_SETTINGS
import ru.futurobot.bot.domain.model.InfoSettingsState.Companion.CALLBACK_QUERY
import ru.futurobot.bot.domain.model.InfoSettingsState.Companion.GREETINGS_SETTINGS
import ru.futurobot.bot.domain.model.InfoSettingsState.Companion.YES_NO_SETTINGS
import ru.futurobot.bot.domain.repository.SettingsRepository
import ru.futurobot.bot.domain.repository.UserInfoRepository
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.toInfoSettingsState
import ru.futurobot.bot.domain.toKeyboard
import javax.inject.Inject

internal class SettingsCallbackQueryHandler
@Inject constructor(
    private val telegramInfoRepository: UserInfoRepository,
    private val settingsRepository: SettingsRepository
) : CallbackQueryHandler {

    override fun handle(
        query: CallbackQuery,
        setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!query.data.startsWith(CALLBACK_QUERY)) {
            return false
        }
        val admins = telegramInfoRepository.getAdmins(query.message.chatId)

        if (admins.none { it.user.id == query.from.id.toInt() }) return true

        val data = query.data.substring(CALLBACK_QUERY.length)
        // Update settings
        val updatedSetting = when (data) {
            BOOBS_SETTINGS -> setting.copy(isBoobsEnabled = !setting.isBoobsEnabled)
            BUTTS_SETTINGS -> setting.copy(isButtsEnabled = !setting.isButtsEnabled)
            BYE_SETTINGS -> setting.copy(isByeEnabled = !setting.isByeEnabled)
            GREETINGS_SETTINGS -> setting.copy(isGreetingsEnabled = !setting.isGreetingsEnabled)
            YES_NO_SETTINGS -> setting.copy(is8BallEnabled = !setting.is8BallEnabled)
            else -> setting
        }
        settingsRepository.update(updatedSetting)

        // Generate settings keyboard and send it
        val settingsState = updatedSetting.toInfoSettingsState()
        val keyboard = settingsState.toKeyboard()
        messageService?.editKeyboard(query.message.chatId, query.message.messageId, keyboard)
        return true
    }
}