package ru.futurobot.bot.domain.model

/**
 * Created by Alexey on 08.04.2018. Futurobot
 */
data class InfoSettingsState(
    val text: String,
    var buttons: List<InfoButton>
) {

    companion object {
        const val CALLBACK_QUERY = "EDIT_SETTINGS"
        const val BOOBS_SETTINGS = "boobs-settings"
        const val BUTTS_SETTINGS = "butts-settings"
        const val GREETINGS_SETTINGS = "greetings-settings"
        const val BYE_SETTINGS = "bye-settings"
        const val YES_NO_SETTINGS = "yes-no-settings"
    }
}
