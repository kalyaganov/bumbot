package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.SettingEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingsDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) :
    DbDataSource<SettingEntity>(sessionFactory, logger) {
    fun readByChatId(chatId: Long): SettingEntity? = sessionFactory.readTransaction {
        session.createQuery("FROM SettingEntity S WHERE S.chat.id = :chatId")
            .setParameter("chatId", chatId)
            .setMaxResults(1)
            .list().firstOrNull() as? SettingEntity
    }
}