package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.ChatEntity
import ru.futurobot.bot.data.entity.StatisticEntity
import ru.futurobot.bot.data.entity.UserEntity
import ru.futurobot.bot.domain.database.StatisticsDataSource
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class StatisticRepository
@Inject constructor(
    private val statisticsDataSource: StatisticsDataSource
) {
    fun addOrUpdate(statistic: StatisticEntity) = statisticsDataSource.createOrUpdate(statistic)

    fun getStatistic(chat: ChatEntity, user: UserEntity): StatisticEntity =
        statisticsDataSource.readForUser(chat.id, user.id) ?: StatisticEntity(chat = chat, user = user)

    fun getByTopMessages(chat: ChatEntity, topCount: Int): Collection<StatisticEntity> =
        statisticsDataSource.readTop(chat.id, topCount)
}