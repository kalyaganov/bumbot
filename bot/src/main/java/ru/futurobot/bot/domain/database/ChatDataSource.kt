package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.ChatEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) : DbDataSource<ChatEntity>(sessionFactory, logger) {

    fun readById(chatId: Long): ChatEntity? = sessionFactory.readTransaction {
        session.createQuery("FROM ChatEntity C WHERE C.id = :chatId")
            .setParameter("chatId", chatId)
            .setMaxResults(1)
            .list().firstOrNull() as? ChatEntity
    }
}