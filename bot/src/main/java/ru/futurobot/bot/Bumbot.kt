package ru.futurobot.bot

import org.telegram.telegrambots.bots.DefaultBotOptions
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.objects.Update
import javax.inject.Inject
import javax.inject.Named

class Bumbot @Inject constructor(
    @Named("botname") private val name: String,
    @Named("token") private val token: String,
    botOptions: DefaultBotOptions = DefaultBotOptions()
) : TelegramLongPollingBot(botOptions) {

    var updateHandler: UpdateHandler? = null

    override fun getBotToken(): String = token
    override fun getBotUsername(): String = name
    override fun onUpdateReceived(update: Update?) {
        if (update != null) updateHandler?.handleMessage(update)
    }
}