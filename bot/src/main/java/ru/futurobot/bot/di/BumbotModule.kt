package ru.futurobot.bot.di

import com.google.inject.AbstractModule
import com.google.inject.Singleton
import ru.futurobot.bot.domain.analytics.FakeAnalyticsService
import ru.futurobot.bot.domain.handlers.CallbackQueryMessageHandlers
import ru.futurobot.bot.domain.handlers.TextMessageHandlers
import ru.futurobot.bot.domain.repository.*
import ru.futurobot.bot.domain.service.AnalyticsService


class BumbotModule : AbstractModule() {
    override fun configure() {
        // Text message handlers
        bind(TextMessageHandlers::class.java)
        bind(CallbackQueryMessageHandlers::class.java)
        bind(AnalyticsService::class.java).to(FakeAnalyticsService::class.java)

        bind(BoobsRepository::class.java).`in`(Singleton::class.java)
        bind(ButtsRepository::class.java).`in`(Singleton::class.java)
        bind(ChatRepository::class.java).`in`(Singleton::class.java)
        bind(FileRepository::class.java).`in`(Singleton::class.java)
        bind(JoinPhraseRepository::class.java).`in`(Singleton::class.java)
        bind(LeavePhraseRepository::class.java).`in`(Singleton::class.java)
        bind(RatingRepository::class.java).`in`(Singleton::class.java)
        bind(SettingsRepository::class.java).`in`(Singleton::class.java)
        bind(StatisticRepository::class.java).`in`(Singleton::class.java)
        bind(UserRepository::class.java).`in`(Singleton::class.java)
        bind(VotePhraseRepository::class.java).`in`(Singleton::class.java)
        bind(UserInfoRepository::class.java).`in`(Singleton::class.java)
    }
}

