package ru.futurobot.bot.di.provider

import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Provider

class LoggerProvider @Inject constructor(
    @Named("logger-name") private val name: String,
    @Named("debug") private val isDebug: Boolean
) : Provider<Logger> {
    override fun get(): Logger = Logger.getLogger(name).apply {
        addHandler(ConsoleHandler())
        level = if (isDebug) Level.ALL else Level.WARNING
        useParentHandlers = false
    }
}