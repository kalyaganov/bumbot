package ru.futurobot.bot.di.provider

import org.apache.http.HttpHost
import org.apache.http.client.config.RequestConfig
import org.telegram.telegrambots.bots.DefaultBotOptions
import ru.futurobot.bot.Bumbot
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Provider

class BumbotProvider @Inject constructor(
    @Named("botname") private val name: String,
    @Named("token") private val token: String,
    @Named("proxy") private val proxy: String
) : Provider<Bumbot> {
    override fun get(): Bumbot {
        val botOptions = DefaultBotOptions().apply {
            if (proxy.isNotEmpty()) {
                requestConfig = RequestConfig.custom()
                    .setProxy(HttpHost.create(proxy))
                    .build()
            }
        }
        return Bumbot(name, token, botOptions)
    }

}