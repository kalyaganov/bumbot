package ru.futurobot.bot.di

import com.google.gson.Gson
import com.google.inject.AbstractModule
import com.google.inject.Singleton
import okhttp3.OkHttpClient
import ru.futurobot.bot.di.provider.OboobsApiProvider
import ru.futurobot.bot.di.provider.ObuttsApiProvider
import ru.futurobot.bot.domain.model.data.OboobsApi
import ru.futurobot.bot.domain.model.data.ObuttsApi
import ru.futurobot.bot.domain.service.DownloadingService

class NetworkModule : AbstractModule() {
    override fun configure() {
        bind(OkHttpClient::class.java).toInstance(OkHttpClient.Builder().build())
        bind(Gson::class.java).toInstance(Gson())

        bind(DownloadingService::class.java).`in`(Singleton::class.java)

        bind(OboobsApi::class.java).toProvider(OboobsApiProvider::class.java).`in`(Singleton::class.java)
        bind(ObuttsApi::class.java).toProvider(ObuttsApiProvider::class.java).`in`(Singleton::class.java)
    }
}