package ru.futurobot.bot.data.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "files")
data class FileEntity(
    @Id
    @Column(name = "file_id", unique = true)
    val id: String = "-1",

    @Column(name = "key")
    val key: String = "key"
)
