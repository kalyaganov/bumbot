package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "greetings_phrases")
data class GreetingsPhraseEntity(
    @Id
    @SequenceGenerator(
        name = "greetings_phrases_seq", sequenceName = "greetings_phrases_greetings_phrase_id_seq",
        allocationSize = 0
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "greetings_phrases_seq")
    @Column(name = "greetings_phrase_id")
    val id: Int = 0,

    @Column(name = "phrase")
    val phrase: String = "",

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "greetings_phrase_chat_id", nullable = true, insertable = true, updatable = false)
    val chat: ChatEntity? = null
)
