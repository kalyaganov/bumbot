package ru.futurobot.bot.data.entity

data class Butt(val id: String, var preview: String, val previewImageName: String = "", var modelName: String? = null) {
    val imageUrl: String
        get() = "http://media.obutts.ru/$preview"
}