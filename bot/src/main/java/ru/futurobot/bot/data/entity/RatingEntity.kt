package ru.futurobot.bot.data.entity

import java.time.Instant
import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "ratings")
data class RatingEntity(

    @Id
    @SequenceGenerator(name = "ratings_seq", sequenceName = "ratings_rating_id_seq", allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ratings_seq")
    @Column(name = "rating_id")
    val id: Int = 0,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = true, updatable = false, nullable = false)
    val user: UserEntity = UserEntity(),

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chat_id", insertable = true, updatable = false, nullable = false)
    val chat: ChatEntity = ChatEntity(),

    @Column(name = "rating")
    val rating: Float = 100.toFloat(),

    @Column(name = "vote_date")
    @Convert(converter = LongToDateTimeConverter::class)
    val voteDate: Instant = Instant.now(),

    @Column(name = "next_vote_date")
    @Convert(converter = LongToDateTimeConverter::class)
    val nextVoteDate: Instant = Instant.now()
)