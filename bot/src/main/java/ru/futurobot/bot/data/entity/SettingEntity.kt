package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "settings")
data class SettingEntity(
    @Id
    @SequenceGenerator(name = "settings_seq", sequenceName = "settings_settings_id_seq", allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "settings_seq")
    @Column(name = "settings_id")
    val id: Int = 0,

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "settings_chat_id", nullable = true, insertable = true, updatable = false)
    val chat: ChatEntity = ChatEntity(),

    @Column(name = "boobs_enabled", nullable = false)
    val isBoobsEnabled: Boolean = true,

    @Column(name = "butts_enabled", nullable = false)
    val isButtsEnabled: Boolean = true,

    @Column(name = "greetings_enabled", nullable = false)
    val isGreetingsEnabled: Boolean = true,

    @Column(name = "bye_enabled", nullable = false)
    val isByeEnabled: Boolean = true,

    @Column(name = "yes_no_enabled", nullable = false)
    val is8BallEnabled: Boolean = true
)