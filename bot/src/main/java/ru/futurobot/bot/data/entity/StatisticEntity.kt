package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "stats")
data class StatisticEntity(
    @Id
    @SequenceGenerator(name = "stats_seq", sequenceName = "stats_stat_id_seq", allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stats_seq")
    @Column(name = "stat_id")
    val id: Int = 0,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false, insertable = true, updatable = false)
    val user: UserEntity = UserEntity(),

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "chat_id", nullable = false, insertable = true, updatable = false)
    val chat: ChatEntity = ChatEntity(),

    @Column(name = "messages")
    val messagesCount: Int = 0,

    @Column(name = "stickers")
    val stickersCount: Int = 0,

    @Column(name = "texts")
    val textsCount: Int = 0,

    @Column(name = "images")
    val imagesCount: Int = 0,

    @Column(name = "documents")
    val documentsCount: Int = 0,

    @Column(name = "audio")
    val audiosCount: Int = 0,

    @Column(name = "video")
    val videosCount: Int = 0,

    @Column(name = "voice")
    val voicesCount: Int = 0
)