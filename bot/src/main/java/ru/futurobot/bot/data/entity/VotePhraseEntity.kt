package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "vote_phrases")
data class VotePhraseEntity(
    @Id
    @SequenceGenerator(
        name = "vote_phrases_seq", sequenceName = "vote_phrases_vote_phrases_id_seq",
        allocationSize = 0
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vote_phrases_seq")
    @Column(name = "vote_phrases_id")
    val id: Int = 0,

    @Column(name = "phrase", nullable = false)
    val phrase: String = "",

    @Column(name = "vote_power")
    val power: Int = 0,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vote_phrases_chat_id", nullable = true, insertable = true, updatable = false)
    val chat: ChatEntity? = null
)

enum class PhraseType { Positive, Negative }