package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "chats")
data class ChatEntity(

    @Id
    @Column(name = "chat_id", unique = true)
    val id: Long = 0,

    @Column(name = "title")
    val title: String = "",

    @Column(name = "username")
    val username: String? = "",

    @Column(name = "first_name")
    val firstName: String? = "",

    @Column(name = "last_name")
    val lastName: String? = "",

    @Column(name = "type")
    val type: Int = 0,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "chat", orphanRemoval = true)
    val leavePhraseEntitySet: Set<LeavePhraseEntity> = emptySet(),

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "chat", orphanRemoval = true)
    val votePhraseEntitySet: Set<VotePhraseEntity> = emptySet(),

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "chat", orphanRemoval = true)
    val greetingsPhraseEntitySet: Set<GreetingsPhraseEntity> = emptySet(),

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "chat", orphanRemoval = true)
    val statisticEntitySet: Set<StatisticEntity> = emptySet(),

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "chat", orphanRemoval = true)
    val ratingEntitySet: Set<RatingEntity> = emptySet(),

    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, mappedBy = "chat", orphanRemoval = true)
    val settingEntity: SettingEntity? = null
)
