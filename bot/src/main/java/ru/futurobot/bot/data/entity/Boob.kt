package ru.futurobot.bot.data.entity

/**
 * Boobs retrofit model
 */
data class Boob(val id: String, var preview: String, val previewImageName: String = "", var modelName: String? = null) {
    val imageUrl: String
        get() = "http://media.oboobs.ru/$preview"
}
