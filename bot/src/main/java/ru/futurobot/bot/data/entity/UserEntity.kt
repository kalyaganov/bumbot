package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "users")
data class UserEntity(
    @Id
    @Column(name = "user_id", unique = true)
    val id: Long = 0,

    @Column(name = "username")
    val username: String? = "",

    @Column(name = "first_name")
    val firstName: String? = "",

    @Column(name = "last_name")
    val lastName: String? = "",

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    val statisticEntitySet: Set<StatisticEntity> = emptySet(),

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    val ratingEntitySet: Set<RatingEntity> = emptySet()
)
