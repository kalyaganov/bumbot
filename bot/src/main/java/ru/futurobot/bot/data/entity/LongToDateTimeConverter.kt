package ru.futurobot.bot.data.entity

import java.time.Instant
import javax.persistence.AttributeConverter
import javax.persistence.Converter

/**
 * Created by Alexey on 07.04.2018. Futurobot
 */
@Converter
class LongToDateTimeConverter : AttributeConverter<Instant, Long> {
    override fun convertToDatabaseColumn(attribute: Instant): Long = attribute.toEpochMilli()

    override fun convertToEntityAttribute(dbData: Long): Instant = Instant.ofEpochMilli(dbData)
}
