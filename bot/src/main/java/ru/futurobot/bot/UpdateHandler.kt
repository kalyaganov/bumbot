package ru.futurobot.bot

import org.telegram.telegrambots.meta.api.objects.Update
import ru.futurobot.bot.domain.service.BumbotService
import ru.futurobot.bot.domain.service.MessageService
import javax.inject.Inject

class UpdateHandler @Inject constructor(
    private val bumbotService: BumbotService,
    private val messageService: MessageService
) {

    fun handleMessage(update: Update) {
        when {
            update.hasMessage() -> bumbotService.handleMessage(update.message, messageService)
            update.hasCallbackQuery() -> bumbotService.handleQueryMessage(update.callbackQuery, messageService)
        }
    }
}