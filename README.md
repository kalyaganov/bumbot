THE BUMBOT - TELEGRAM BOT
---

#### Handlers

There are different handlers for text messages:
- `BoobsMessageHandler` react on boobs-mention text messages and throws good looking boobs picture to chat
- `ButtsMessageHandler` same for butts
- `EightBallMessageHandler` react to asking questions, for example reply any bot message and ask "Should i drink vodka today?". Bot will help you to make decision.
- `HelpCommandMessageHandler` just help information
- `MyStatCommandHandler` provide you information about your statistics at given chat
- `VoteCommandMessageHandler` allow users to rate each others message. User need to reply some message and send pluses("+++") or minuses("---")
- `SessingsCommandHandler` show admin settings message for given chat. Admin can switch on/off boobs, greetings, butts and other stuff
- `StatCommandHandler` show statistics for given chat
- `TitleChangedMessageHandler` react to every title change
- `TopCommandHandler` show top five users of chat with high rating
- `UserJoinMessageHandler` greetings for new user
- `UserLeaveMessageHandler` same for user leaving
- `RateMessageHandler` rate any message. Just reply message and call /rate command or just question mark. Bot will provide his opinion

To add new `MessageHandler` implement it by extension of `MessageHandler` class and add new handler to `TextMessageHandlers` class 

#### Configuration
- In `bot/src/main/resource/application.properties` specify your bot username and token. 
New bot can be created via `BotFather` https://t.me/BotFather
- In `bot/src/main/resource/hibernate.properties` specify your connection settings. This project uses postgresql database. If you need to change database, then you need to add your database driver dependency at `bot/build.gradle` and change `dialect`, `driver_class` and other stuff at hibernate config file
- That`s it

#### Deploy
- Run gradle task to generate jar file. You can run it in your IDE if it support gradle, or manual via console. `./gradlew[.bat] :bot:build`. You will get output jar file at `bot/build/libs`
- Copy your jar file to your server
- Place your production `application.properties` file near your jar file
- Place your production `hibernate.properties` file somewhere
- Add new line to your `application.properties` file `hibernate=<path/to/hibernate.properties>`
- Run bot with command `java -jar bot.jar --properties=<path/to/bot.properties>`
- That`s it